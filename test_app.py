import unittest
from app import app


class Testpp(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_home(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200, "Should be 200")

    def test_cv(self):
        result = self.app.get('/cv')
        self.assertEqual(result.status_code, 200, "Should be 200")

    def test_new(self):
        result = self.app.get('/new')
        self.assertEqual(result.status_code, 200, "Should be 200")


if __name__ == '__main__':
    unittest.main()
