from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
@app.route('/index')
def index():
    return render_template("index.html")

@app.route("/cv")
def cv():
    page_name = 'cv'
    return render_template("cv.html", page_name=page_name)

@app.route("/new")
def new():
    return render_template("new.html")

if __name__ == "__main__":
    app.run(debug=True)
